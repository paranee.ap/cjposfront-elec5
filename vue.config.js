module.exports = {
  transpileDependencies: ['vuex-module-decorators'],
  runtimeCompiler: true,
  pluginOptions: {
    electronBuilder: {
      // List native deps here if they don't work
      externals: ['escpos'],
      // If you are using Yarn Workspaces, you may have multiple node_modules folders
      // List them all here so that VCP Electron Builder can find them
      nodeModulesPath: ['../../node_modules', './node_modules'],
    },
  },
  css: {
    loaderOptions: {
      less: {
        modifyVars: {
          // 'primary-color': '#1DA57A',
          'link-color': '#1DA57A',
          // 'border-radius-base': '2px',
        },
        javascriptEnabled: true,
      },
      sass: {
        data: `
          @import "@/style/_var.scss";
        `,
      },
    },
  },
  chainWebpack: (config) => {
    // config.plugin('copy').use(require('copy-webpack-plugin'), [[
    //   { from: '/src/*', to: 'public/', toType: 'dir' }
    // ]])

  },
};
