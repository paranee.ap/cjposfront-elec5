import { Printer } from './printer_i';
import { PrinterRow } from './printer_row_i';

const escpos = require('escpos');

export class USBPrinter implements Printer {
    private _printer: any;

    private _device: any;

    private options = {
      encoding: '874',
    }

    // constructor
    constructor(options: any = null) {
      if (options) { this.options = options; }
    }

    // public method
    public print(printerRow: PrinterRow[]): void {
      const device = new escpos.USB();
      const printer = new escpos.Printer(device, this.options);
      device.open(() => {
        printer
          .font('a')
          .align('ct')
          .style('bu');

        printerRow.forEach((row) => {
          if (row.font) { printer.font(row.font); }
          if (row.align) { printer.align(row.align); }
          if (row.style) { printer.style(row.style); }
          if (row.width && row.height) { printer.size(row.width, row.height); }
          if (row.text) {
            printer.text(row.text);
          } else if (row.barcode) {
            row.barcodeType ? printer.barcode(row.barcode) : printer.barcode(row.barcode, row.barcodeType);
          } else if (row.qrcode) {
            printer.qrimage(row.qrcode);
          } else {
            printer.text('');
          }
        });
        printer
          .text('')
          .text('')
          .text('')
          .cut()
          .close();
      });
    }
}
