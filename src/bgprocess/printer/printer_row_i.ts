export interface PrinterRow{
    font?:string;
    align?:string;
    style?:string;
    width?:number;
    height?:number;
    text?:string;
    barcode?:string;
    barcodeType?:string;
    qrcode?:string;
}
