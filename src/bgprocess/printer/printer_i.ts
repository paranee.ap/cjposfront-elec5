import { PrinterRow } from './printer_row_i';

export interface Printer{
    print(printerRow: PrinterRow[]):void;
}
