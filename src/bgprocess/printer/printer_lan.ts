import { Printer } from './printer_i';
import { PrinterRow } from './printer_row_i';

const escpos = require('escpos');

export class LanPrinter implements Printer {
    // declair variable
    private _ip: string;

    private _port: number;

    private options = {
      encoding: 'tis620',
    }

    // setter
    set ip(_ip: string) {
      this._ip = _ip;
    }

    set port(_port: number) {
      this._port = _port;
    }

    // constructor
    constructor(ip: string = 'localhost', port: number = 9100, options: any = null) {
      this._ip = ip;
      this._port = port;

      if (options) { this.options = options; }
    }

    // public method
    public print(printerRow: PrinterRow[]): void {
      const device = new escpos.Network(this._ip);
      const printer = new escpos.Printer(device, this.options);
      device.open(() => {
        printer
          .font('a')
          .align('ct')
          .style('bu');

        printerRow.forEach((row) => {
          if (row.font) { printer.font(row.font); }
          if (row.align) { printer.align(row.align); }
          if (row.style) { printer.style(row.style); }
          if (row.width && row.height) { printer.size(row.width, row.height); }
          if (row.text) {
            printer.text(row.text);
          } else if (row.barcode) {
            row.barcodeType ? printer.barcode(row.barcode) : printer.barcode(row.barcode, row.barcodeType);
          } else if (row.qrcode) {
            printer.qrimage(row.qrcode);
          } else {
            printer.text('');
          }
        });
        printer
          .text('')
          .text('')
          .text('')
          .cut()
          .close();
      });
    }

  // private method
}
