// import * as escpos from 'escpos'
import {
  Printer,
  PrinterRow,
  LanPrinter,
  USBPrinter,
} from './printer/printer.module';

const { ipcMain } = require('electron');

ipcMain.on('asynchronous-message', (event: any, arg: any) => {
  console.log(arg); // prints "ping"
  event.sender.send('asynchronous-reply', 'pong');
});

ipcMain.on('synchronous-message', (event: any, arg: any) => {
  console.log(arg); // prints "ping"
  event.returnValue = 'pong';
});

ipcMain.on('print', (event: any, arg: any) => {
  console.log('print text', arg);
  const row: PrinterRow[] = arg;
  const printer: Printer = new LanPrinter('192.168.137.222');
  // let printer:Printer = new USBPrinter();
  printer.print(row);
});
