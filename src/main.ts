import Vue from 'vue';
import './plugins/axios';

import '@/permission';

import ElementUI from 'element-ui';
import './style/styles.scss';
// import 'bootstrap';
import '@/style/bootstrap.css';
import '@/style/global.scss';

// @ts-ignore
import lang from 'element-ui/lib/locale/lang/en';
// @ts-ignore
import locale from 'element-ui/lib/locale';

import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(ElementUI);
// configure language
locale.use(lang);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
