import { Vue } from 'vue-property-decorator';

export default class BaseComponent extends Vue {
  showNotify(text:string) {
    this.$root.$emit('notify', text);
  }
}
