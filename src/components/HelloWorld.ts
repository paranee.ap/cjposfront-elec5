import { Component, Prop, Vue } from 'vue-property-decorator';
import { localDB } from '@/db/db_manager';

@Component
export default class HelloWorld extends Vue {
  @Prop() private msg!: string;

  dbClick() {
    // var doc = {
    //   "_id": "mittens",
    //   "name": "Mittens",
    //   "occupation": "kitten",
    //   "age": 3,
    //   "hobbies": [
    //     "playing with balls of yarn",
    //     "chasing laser pointers",
    //     "lookin' hella cute"
    //   ]
    // };

    // localDB.put(doc)

    // localDB.info().then(function (info) {
    //   console.log(info);
    // })

    localDB.get('0001').catch((err) => {
      if (err.name === 'not_found') {
        return {
          _id: 'default',
          name: 'Mittens',
          occupation: 'kitten',
          age: 3,
        };
      }
    }).then((doc) => {
      // okay, doc contains our document
      console.log(doc);
    }).catch((err) => {
      // oh noes! we got an error
      console.log('error na ja', err);
    });

    this.getAllData().then(docs => console.log(docs)).catch(err => console.log(err));
    console.log('in the end');
  }

  async getAllData() {
    try {
      const docs = await localDB.allDocs();
      return docs;
    } catch (err) {
      return err;
    }
  }
}
