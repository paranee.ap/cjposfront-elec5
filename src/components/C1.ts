import { Component, Prop, Vue } from 'vue-property-decorator';
import { PrinterRow } from '@/bgprocess/printer/printer.module';
import { ipcRenderer } from 'electron';
import { METHODS } from 'http';

// The @Component decorator indicates the class is a Vue component
@Component
export default class C1 extends Vue {
  // Initial data can be declared as instance properties
  message: string = 'Hello! Test printer'

  @Prop() quantity: number = 0

  // Component methods can be declared as instance methods
  created() {

  }


  onClick(): void {
    const row: PrinterRow[] = [];

    const row1: PrinterRow = {
      align: 'CT',
      text: 'hello world!!!!',
    };
    row.push(row1);

    const row2: PrinterRow = {
      align: 'CT',
      text: 'สวัสดีครัช',
    };
    row.push(row2);

    ipcRenderer.send('print', row);
    window.alert(this.message);

    this.quantity += 1;
  }
}
