// import axios from 'axios';
import {
  UserSubmit,
  UserResponse,
  User,
  Profile,
  ProfileResponse,
  UserForUpdate,
} from './models';
import { setToken, removeToken } from '@/utils/auth';

// export const conduitApi = axios.create({
//   baseURL: 'https://conduit.productionready.io/api',
// });

export function setJWT(jwt: string) {
  // conduitApi.defaults.headers.common['Authorization'] = `Token ${jwt}`;
}

export function clearJWT() {
  // delete conduitApi.defaults.headers.common['Authorization'];
}

export async function loginUser(user: UserSubmit): Promise<User> {
  const token = 'eieikiki000111245643';

  return new Promise(resolve => requestAnimationFrame(() => {
    setToken(token);
    const u = <User>{ token };
    resolve(u);
  }));
  // const response = await conduitApi.post('/users/login', {
  //   user,
  // });
  // return (response.data as UserResponse).user;
}

export async function fetchProfile(username: string): Promise<Profile> {
  // const response = await conduitApi.get(`/profiles/${username}`);
  // return (response.data as ProfileResponse).profile;
  await new Promise(resolve => requestAnimationFrame(resolve));
  const p:Profile = <Profile>{};
  return p;
}

export async function fetchUser(): Promise<User> {
  // const response = await conduitApi.get('/user')
  // return (response.data as UserResponse).user
  await new Promise(resolve => requestAnimationFrame(resolve));
  const u:User = <User>{};
  return u;
}

export async function updateUser(user: UserForUpdate): Promise<User> {
  // const response = await conduitApi.put('/user', user)
  // return (response.data as UserResponse).user

  await new Promise(resolve => requestAnimationFrame(resolve));
  const u:User = <User>{};
  return u;
}

export async function checkAuthen(): Promise<User> {
  // const response = await conduitApi.put('/user', user)
  // return (response.data as UserResponse).user
  await new Promise(resolve => requestAnimationFrame(resolve));
  const u:User = <User>{};
  return u;
}

export function logout() {
  removeToken();
}
