import {
  getModule,
  Module,
  MutationAction,
  VuexModule,
  Action,
  Mutation,
} from 'vuex-module-decorators';
import store from '@/store';
import {
  Profile, User, UserSubmit, UserForUpdate,
} from './models';
import * as auth from './auth.service';
import { removeToken } from '@/utils/auth';

@Module({
  namespaced: true,
  name: 'users',
  store,
  dynamic: true,
})
class UsersModule extends VuexModule {
  user: User | null = null;

  profile: Profile | null = null;

  token:string = ''

  get username() {
    return (this.user && this.user.username) || null;
  }

  @MutationAction
  async login(userSubmit: UserSubmit) {
    const user = await auth.loginUser(userSubmit);
    //   setJWT(user.token)
    return { user };
  }

  // @Action({ commit: 'SET_TOKEN' })
  // async logout() {
  //   console.log('action logout')
  //   removeToken();
  //   return '';
  // }

  // @Mutation
  // SET_TOKEN(token: string) {
  //   this.token = token;
  // }

  @MutationAction
  async logout() {
    return new Promise((resolve) => {
      removeToken();
      resolve('');
    });
  }

  @MutationAction
  async loadProfile(username: string) {
    const profile = await auth.fetchProfile(username);
    return { profile };
  }

  @MutationAction
  async loadUser() {
    const user = await auth.fetchUser();
    return { user };
  }

  @MutationAction
  async updateSelfProfile(userUpdateFields: UserForUpdate) {
    const user = await auth.updateUser(userUpdateFields);
    return { user };
  }

  @MutationAction
  async checkAuthen() {
    console.log('action checkAuthen');
    const user = await auth.checkAuthen();
    return { user };
  }
}

export default getModule(UsersModule);
