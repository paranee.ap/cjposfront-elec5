export interface ProductBarcode {
  name: string;
  barcode?: string;
  price: any;
  qty: any;
  sum: any;
}
