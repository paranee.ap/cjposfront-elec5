import { Route } from 'vue-router';
import router from './router';
import store from './store';
import UserModule from '@/store/auth/user.module';
import { getToken } from './utils/auth';

const whiteList = ['/login'];

router.beforeEach((to, from, next) => {
  if (getToken()) {
    if (to.path === '/login') {
      next({ path: '/' });
      // NProgress.done(); // If current page is dashboard will not trigger afterEach hook, so manually handle it
    } else {
      // if (UserModule.roles.length === 0) {
      //     UserModule.GetInfo().then(() => {
      //         next();
      //     }).catch((err) => {
      //         UserModule.FedLogOut().then(() => {
      //             Message.error(err || '授权失败，请重新登录');
      //             next({ path: '/login' });
      //         });
      //     });
      // } else {
      //     next();
      // }
      console.log('go to ', to.path);
      next();
    }
  } else {
    console.log('token not found');
    if (whiteList.indexOf(to.path) !== -1) {
      next();
    } else if (to.path === '/login' || from.path === '/login') {
      // NProgress.done();
    } else {
      next('/login');
    }
  }
});
