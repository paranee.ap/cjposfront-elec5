export const PouchDB = jest.genMockFromModule('pouchdb');

export function get(){
    //return Promise<Core.Document<Content & Model> & Core.GetMeta>
    return Promise.resolve(new Response({
        _id: 'default',
        name: 'Mittens',
        occupation: 'kitten',
        age: 3,
      }));
}

export function allDocs(){
    return Promise.resolve(new Response({
        _id: 'default',
        name: 'Mittens',
        occupation: 'kitten',
        age: 3,
      }));
}