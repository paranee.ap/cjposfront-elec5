var Application = require('spectron').Application
var assert = require('assert')

var app = new Application({
  path: './dist_electron/mac/cjp.app/Contents/MacOS/cjp'
})

app.start().then(function () {
  // Check if the window is visible
  return app.browserWindow.isVisible()
}).then(function (isVisible) {
  // Verify the window is visible
  assert.equal(isVisible, true)
}).then(function () {
  // Get the window's title
  return app.client.getTitle()
}).then(function (title) {
  // Verify the window's title
  assert.equal(title, 'POS Front2')
})
// .then(function () {
//   // Stop the application

//   return app.stop()
// })
.catch(function (error) {
  // Log any failures
  console.error('Test failed', error.message)
})
// .then(function () {

//     var value = $('#nav > a.router-link-exact-active.router-link-active').getValue();
//     console.log("ggf "+value)
//     return app.client.element('#nav > a.router-link-exact-active.router-link-active');
// })
// .then(function(result) {
//     console.log("gg "+result.value)
    
//   })
// .catch(function (error) {
//     // Log any failures
//     console.error('Test failed2', error.message)
//   })
.then(function () {
    // Get the window's title
    return app.client.windowHandles()
}).then((handles) => {
//const windowA = handles.values[0]; // throwing error "can not read property 0 of undefined".
//const windowB = handles.values[1]; // Its a typo I guess, value is the correct field not values

console.log(handles);

  const windowA = handles.value[0];
  const windowB = handles.value[1];

  //'#nav > a.router-link-exact-active.router-link-active'
    console.log("ggg "+windowA.title+" "+windowB.title)
    windowA.focus()
  app.client.window(windowA).click('#nav > a.router-link-exact-active.router-link-active');  // will click element of windowA
  app.client.window(windowB).click('#nav > a.router-link-exact-active.router-link-active');  // will click element of windowB
})
.then(function () {
  // Stop the application

  return app.stop()
});