import { mount, createLocalVue } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'
// import pouchdb from 'pouchdb'
// import { get, allDocs } from '../../mocks/PouchDB'
// import { localDB } from '../../mocks/db_manager'
// const localVue = createLocalVue()
import { getToken, setToken, removeToken } from '@/utils/auth'

// // install plugins as normal
// localVue.use(BootstrapVue)

// // pass the `localVue` to the mount options
// mount(Component, {
//   localVue
// })
// jest.mock('../../mocks/db_manager')
// jest.mock('pouchdb');

describe('testo', () => {
//   test('is a Vue instance', () => {
//     const wrapper = mount(HelloWorld)
//     expect(wrapper.isVueInstance()).toBeTruthy()
//   })


test("Auth Token must valid", () => {
  setToken("hihi")
  expect(window.sessionStorage.getItem('token')).toEqual("hihi")
})

  test("Auth GetToken Test", () => {
    setToken("hihi")
    expect(getToken()).toEqual("hihi")
  })

  test("Auth RemoveToken Test", () => {
    setToken("hihi")
    expect(getToken()).toEqual("hihi")
    removeToken()
    expect(getToken()).toBeNull()
  })

  


})