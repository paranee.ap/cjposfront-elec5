const { testWithSpectron } = require('vue-cli-plugin-electron-builder');

jest.setTimeout(50000);
jest
  .dontMock('jquery');
var $ = require('jquery');

test('Window Loads Properly', async () => {
  // Wait for dev server to start
  const { app, stopServe } = await testWithSpectron();
  const win = app.browserWindow;
  const { client } = app;

  // Window was created
  expect(await client.getWindowCount()).toBe(1);
  // It is not minimized
  expect(await win.isMinimized()).toBe(false);
  // Window is visible
  expect(await win.isVisible()).toBe(true);
  // Size is correct
  const { width, height } = await win.getBounds();
  expect(width).toBeGreaterThan(0);
  expect(height).toBeGreaterThan(0);
  // App is loaded properly
  // expect(
  //   /Welcome to Your Vue\.js (\+ TypeScript )?App/.test(
      
  //     await client.getHTML('#app'),
  //   ),
  // ).toBe(true);

  
  $('#app > div > div > div > div > form > fieldset:nth-child(1) > div > input').value = 'Peter';
  $('#app > div > div > div > div > form > fieldset:nth-child(2) > div > input').value = 'Parker';
  // expect($('#app > div > div > div > div > form > fieldset:nth-child(1) > div > input').value).toBe('Peter');
  // expect($('#app > div > div > div > div > form > fieldset:nth-child(2) > div > input').value).toBe('Parker');

  $('#app > div > div > div > div > form > button').submit();

  expect($('#app > div > div > div > div > form > fieldset:nth-child(1) > div > input')).not.toBeNull();

  await stopServe();
});
